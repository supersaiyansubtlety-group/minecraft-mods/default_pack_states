package net.sssubtlety.default_pack_states;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import static net.sssubtlety.default_pack_states.DefaultPackStates.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.Gui.Tooltip()
    boolean fetch_translation_updates = FeatureControl.Defaults.fetch_translation_updates;
}
