package net.sssubtlety.default_pack_states.mixin;

import com.llamalad7.mixinextras.injector.WrapWithCondition;
import net.minecraft.client.option.GameOptions;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import java.util.Set;

@Mixin(GameOptions.class)
public class GameOptionsMixin {
	@WrapWithCondition(method = "addResourcePackProfilesToManager", at = @At(value = "INVOKE", target = "Ljava/util/Set;add(Ljava/lang/Object;)Z"))
	private boolean default_pack_states$FilterAddedProfiles(Set<?> packNames, Object packNameO) {
		return true;
	}
}
