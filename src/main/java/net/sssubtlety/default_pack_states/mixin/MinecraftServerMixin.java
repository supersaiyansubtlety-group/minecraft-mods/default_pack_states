package net.sssubtlety.default_pack_states.mixin;

import com.google.common.collect.ImmutableList;
import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.WrapWithCondition;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.resource.pack.DataPackSettings;
import net.minecraft.resource.pack.ResourcePackManager;
import net.minecraft.server.MinecraftServer;
import net.sssubtlety.default_pack_states.DefaultPackStates;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Slice;

import java.util.List;
import java.util.Set;

@Mixin(value = MinecraftServer.class, priority = 500)
public class MinecraftServerMixin {
	@ModifyExpressionValue(method = "loadDataPacks", at = @At(value = "INVOKE", target = "Lnet/minecraft/resource/pack/ResourcePackProfile;getName()Ljava/lang/String;"))
	private static String default_pack_states$copyUnmodifiedDisabled(String name, @Share("packName") LocalRef<String> packName) {
		packName.set(name);
		return name;
	}

	@ModifyExpressionValue(method = "loadDataPacks", at = @At(value = "INVOKE", target = "Ljava/util/List;contains(Ljava/lang/Object;)Z"))
	private static boolean default_pack_states$checkDisabledPacks(boolean contained, ResourcePackManager resourcePackManager, DataPackSettings dataPackSettings, boolean safeMode, @Share("packName") LocalRef<String> packName) {
		if (contained && !dataPackSettings.getDisabled().contains(packName.get())) {
			DefaultPackStates.LOGGER.debug("Found altered disabled pack: " + packName.get());
		}
		return contained;
	}

	@WrapWithCondition(method = "loadDataPacks", at = @At(value = "INVOKE", target = "Lorg/slf4j/Logger;info(Ljava/lang/String;Ljava/lang/Object;)V"))
	private static boolean default_pack_states$preventLogIfDisabling(Logger logger, String msg, Object packNameO, @Share("disabling") LocalBooleanRef disabling) {
		disabling.set(false);
		return !disabling.get();
	}

	@WrapWithCondition(method = "loadDataPacks", at = @At(value = "INVOKE", target = "Ljava/util/Set;add(Ljava/lang/Object;)Z"),
			slice = @Slice(
					from = @At(value = "INVOKE", target = "Lorg/slf4j/Logger;info(Ljava/lang/String;Ljava/lang/Object;)V"),
					to = @At(value = "INVOKE", target = "Ljava/util/Set;isEmpty()Z")
			)
	)
	private static boolean default_pack_states$preventAddIfDisabling(Set<?> enabled, Object packNameO, @Share("disabling") LocalBooleanRef disabling) {
		return !disabling.get();
	}

//	@WrapOperation(method = "loadDataPacks", at = @At(value = "INVOKE", target = "Ljava/util/List;contains(Ljava/lang/Object;)Z"))
//	private static boolean default_pack_states$checkDisabledPacks(List<?> disabled, Object packNameO, Operation<List<?>> contains) {
//		if (contains.call(packNameO))
//	}
}
