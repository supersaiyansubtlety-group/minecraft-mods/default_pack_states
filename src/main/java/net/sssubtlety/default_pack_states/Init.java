package net.sssubtlety.default_pack_states;

import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;

public class Init implements ModInitializer {
	@Override
	public void onInitialize(ModContainer thisMod) {
		FeatureControl.init();
	}
}
