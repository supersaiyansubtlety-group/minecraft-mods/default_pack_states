package net.sssubtlety.default_pack_states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPackStates {
	public static final String NAMESPACE = "default_pack_states";
	public static final Util.TranslatableString NAME = new Util.TranslatableString("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LoggerFactory.getLogger(NAME.get());

}
