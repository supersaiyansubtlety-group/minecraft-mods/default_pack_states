package net.sssubtlety.default_pack_states;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer;

import static net.sssubtlety.default_pack_states.FeatureControl.shouldFetchTranslationUpdates;

public class ClientInit implements ClientModInitializer {
	@Override
	public void onInitializeClient(ModContainer thisMod) {
		if (shouldFetchTranslationUpdates())
			CrowdinTranslate.downloadTranslations("mod-id", DefaultPackStates.NAMESPACE);
	}
}
